---
title: "Santiago Ramón y Cajal"
date: 2022-12-03
slug: mn408364
thumbnailImagePosition: left
thumbnailImage: images/mn408364.jpg
metaAlignment: center
categories:
- monumento
tags:
- facil

---

{{< alert warning >}}
Ruta de *1900* metros (aprox)
{{< /alert >}}

<!--more-->

{{< image classes="clean center" src="/images/mn408364.jpg">}}

{{< blockquote >}}
Es uno de los varios homenajes que el Ayuntamiento de Madrid ha dispensado al doctor Ramón y Cajal en diferentes lugares de la ciudad, en este caso en el palacete en que vivió, frente al Parque del Retiro, el que él mismo promovió en 1911, con proyecto del arquitecto Julio Martínez-Zapata, y amplió siete años después, esta vez con Ricardo García Guereta. Santiago Ramón y Cajal, premio Nóbel de Fis...
{{< /blockquote >}}

{{< line_break >}}

{{< image classes="clean center" src="/images/mapa-mn408364.png">}}

1. Santiago Ramón y Cajal (C Alfonso XII 64)


2. Observatorio Astronómico (CALLE ALFONSO XII 3), a 150 metros

3. Ministerio de Agricultura, Pesca y Alimentación (PASEO INFANTA ISABEL 1), a 210 metros

4. Pío Baroja (C Claudio Moyano ), a 180 metros

5. Puerta del Ángel Caído del Retiro (C Alfonso XII 5), a 50 metros

6. Día y Noche (   ATOCHA ARGANZUELA), a 410 metros

7. Fuente nueva de la Alcachofa (Pza Emperador Carlos V ), a 370 metros

8. Estación de Atocha (PLAZA EMPERADOR CARLOS V 3), a 90 metros

9. Cavanilles (Pza Murillo 2), a 290 metros

10. Claudio Moyano (C Claudio Moyano ), a 120 metros

11. Niños con jarrón en Atocha (Pza Emperador Carlos V ), a 30 metros


{{< line_break >}}

* [Ficha completa](/pdf/mn408364.pdf)

## OpenData

La información mostrada en esta página procede de los datos abiertos del Ayuntamiento De Madrid

## Subscribete

Si deseas recibir una nueva ruta cada semana en tu correo [subscribete aquí](https://tinyletter.com/rutas-por-madrid)
