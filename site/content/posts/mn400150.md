---
title: "Gallia"
date: 2022-12-03
slug: mn400150
thumbnailImagePosition: left
thumbnailImage: images/mn400150.jpg
metaAlignment: center
categories:
- monumento
tags:
- facil

---

{{< alert warning >}}
Ruta de *440* metros (aprox)
{{< /alert >}}

<!--more-->

{{< image classes="clean center" src="/images/mn400150.jpg">}}

{{< blockquote >}}
Entre los años 1964 y 1968 se levantó en una manzana entre las calles Guzmán el Bueno, Julián Romea, General Rodrigo y General Dávila, un conjunto de viviendas, oficinas y comercio, proyectado por el arquitecto onubense Eleuterio Población Knappe, que cuenta con un pasaje comercial y un área de jardines y zonas libres donde se sitúan varias fuentes, un reloj floral y una serie de esculturas orname...
{{< /blockquote >}}

{{< line_break >}}

{{< image classes="clean center" src="/images/mapa-mn400150.png">}}

1. Gallia (C Guzmán el Bueno 133)


2. Britannia (C Guzmán el Bueno 133), a 30 metros

3. Fuente del parque de las Naciones (C Guzmán el Bueno 133), a 40 metros

4. Fuente de las Naciones (C Guzmán el Bueno 133), a 70 metros

5. América (CALLE GUZMAN EL BUENO 133), a 40 metros

6. Germania (CALLE GUZMAN EL BUENO 133), a 0 metros

7. Hiberia (CALLE GUZMAN EL BUENO 133), a 0 metros

8. Skandia (CALLE GUZMAN EL BUENO 133), a 0 metros

9. Abundancia (CALLE GUZMAN EL BUENO 137), a 140 metros

10. Fortuna (CALLE GUZMAN EL BUENO 137), a 0 metros

11. Maternidad (CALLE GENERAL RODRIGO 8), a 120 metros


{{< line_break >}}

* [Ficha completa](/pdf/mn400150.pdf)

## OpenData

La información mostrada en esta página procede de los datos abiertos del Ayuntamiento De Madrid

## Subscribete

Si deseas recibir una nueva ruta cada semana en tu correo [subscribete aquí](https://tinyletter.com/rutas-por-madrid)
