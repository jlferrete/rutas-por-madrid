---
title: "Maqueta Centenario de la Gran Vía"
date: 2022-12-03
slug: mn409820
thumbnailImagePosition: left
thumbnailImage: images/mn409820.jpg
metaAlignment: center
categories:
- monumento
tags:
- facil

---

{{< alert warning >}}
Ruta de *710* metros (aprox)
{{< /alert >}}

<!--more-->

{{< image classes="clean center" src="/images/mn409820.jpg">}}

{{< blockquote >}}
La maqueta Gran Vía rememora los cien años del comienzo de los trabajos de demolición y posterior apertura de la avenida que debía conectar el este y oeste de Madrid, atravesando su angosto centro histórico. El 5 de abril de 2010 y emulando el acto en el que el Rey Alfonso XIII dio comienzo a la construcción de la Gran Vía con el derribo de la llamada Casa del Párroco, aledaña a la iglesia de San ...
{{< /blockquote >}}

{{< line_break >}}

{{< image classes="clean center" src="/images/mapa-mn409820.png">}}

1. Maqueta Centenario de la Gran Vía (C Gran Vía 1)


2. Casa del cura de la iglesia de San José (Calle ALCALÁ), a 40 metros

3. Edificio Metrópolis (CALLE ALCALA 39), a 70 metros

4. Iglesia de San José (Calle ALCALÁ 43), a 90 metros

5. Conde de Peñalver (C Gran Vía 2), a 50 metros

6. Edificio Gran Peña (Calle GRAN VIA 2), a 20 metros

7. Círculo de Bellas Artes (Calle ALCALÁ 42), a 150 metros

8. Reforma del antiguo edificio de Unión Eléctrica (CALLE GRAN VIA 4), a 160 metros

9. Edificio para el Banco Urquijo (Calle GRAN VIA 4), a 20 metros

10. Viviendas para Luis Ocharán Mazas (Calle CABALLERO DE GRACIA 21), a 60 metros

11. Viviendas para el marqués de Urquijo (Calle GRAN VIA 6), a 50 metros


{{< line_break >}}

* [Ficha completa](/pdf/mn409820.pdf)

## OpenData

La información mostrada en esta página procede de los datos abiertos del Ayuntamiento De Madrid

## Subscribete

Si deseas recibir una nueva ruta cada semana en tu correo [subscribete aquí](https://tinyletter.com/rutas-por-madrid)
