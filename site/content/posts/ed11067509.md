---
title: "Parroquia de Nuestra Señora del Tránsito"
date: 2022-12-23
slug: ed11067509
thumbnailImagePosition: left
thumbnailImage: images/ed11067509.jpg
metaAlignment: center
categories:
- null
tags:
- media

---

{{< alert warning >}}
Ruta de *3800* metros (aprox)
{{< /alert >}}

<!--more-->

{{< image classes="clean center" src="/images/ed11067509.jpg">}}

{{< blockquote >}}
descripcion no disponible
{{< /blockquote >}}

{{< line_break >}}

{{< image classes="clean center" src="/images/mapa-ed11067509.png">}}

{{< line_break >}}

[Ficha completa](/pdf/ed11067509.pdf)

[Ver Mapa](/maps/ruta-ed11067509.html)

1. Parroquia de Nuestra Señora del Tránsito (CARRETERA CANILLAS 40)


2. Reina Sofía (Ctra Canillas 51), a 450 metros

3. Centro de Educación Especial Reina Sofía (Ctra Canillas 51), a 20 metros

4. Fuente de la Junta Municipal de Hortaleza (Ctra Canillas 2), a 980 metros

5. Columnas ornamentales en el jardín del palacete Villa Rosa (   CANILLAS HORTALEZA), a 40 metros

6. Monumento Junta Municipal de Hortaleza (Ctra Canillas 2), a 20 metros

7. Palacete Villa Rosa (Ctra Canillas 2), a 30 metros

8. Rehabilitación jardines de la J. M. Hortaleza (Ctra Canillas 2), a 40 metros

9. Fuente Pilar Miró (Gta Pilar Miró ), a 40 metros

10. Fuente plaza José María Escrivá de Balaguer (Pza José María Escrivá de Balaguer ), a 1330 metros

11. Fuente del Liceo (Pza Liceo ), a 850 metros



## OpenData

La información mostrada en esta página procede de los datos abiertos del Ayuntamiento De Madrid

## Subscribete

Si deseas recibir una nueva ruta cada semana en tu correo [subscribete aquí](https://tinyletter.com/rutas-por-madrid)

También puedes unirte al canal de [Telegram](https://t.me/rutaspormadrid) 

