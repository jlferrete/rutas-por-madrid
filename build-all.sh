#!/bin/bash
for dir in build/*; do
  dir=""${dir##*/}""
  echo "$dir"
  groovy -cp scripts scripts/pdf.groovy $dir
  groovy -cp scripts scripts/blog.groovy $dir
  groovy -cp scripts scripts/map.groovy $dir
  groovy -cp scripts scripts/letter.groovy $dir
  groovy -cp scripts scripts/telegram.groovy $dir
done