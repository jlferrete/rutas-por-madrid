@GrabResolver(name="google",root="https://maven.google.com/")
@Grab(group='com.mapbox.mapboxsdk', module='mapbox-sdk-geojson', version='5.8.0')
@Grab(group='com.mapbox.mapboxsdk', module='mapbox-sdk-services', version='5.8.0')
@Grab(group='androidx.annotation', module='annotation', version='1.5.0')

import com.mapbox.geojson.*
import com.mapbox.api.staticmap.v1.*
import com.mapbox.api.staticmap.v1.models.*
import groovy.text.GStringTemplateEngine
import util.ParsePage
import util.LoadPojos
import util.Model

assert args.length > 0

pojos = new LoadPojos()
list = pojos.list(args[0])
starting = list[0]
parser = new ParsePage()
parser.parsePage(starting)

model = new Model()
model.letterFile(starting.id).text = Eval.me('model', model.model(list), new File('templates/newsletter.groovy').text)
