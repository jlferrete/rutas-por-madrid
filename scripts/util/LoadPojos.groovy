package util

import groovy.xml.XmlParser

import java.nio.file.Files
import java.nio.file.Path

class LoadPojos {
    def pojos = []

    int metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        (meters.round(2) * 1000) as int
    }

    def random() {
        init()
        def list = new File("list.txt").text.split('\n')

        def starting = pojos[new Random().nextInt(pojos.size())]
        while ( list.contains(starting.id) ) {
            starting = pojos[new Random().nextInt(pojos.size())]
        }
        starting
    }

    List list(String id) {
        init()
        def starting = pojos.find { it.id == id }
        def list = pojos.sort(false, { point ->
            metersTo(starting.latitud, starting.longitud, point.latitud, point.longitud)
        })
        list = list[0..10]
        list.eachWithIndex { item, idx ->
            int size = idx ? 150 : 400
            item.descripcion = item.descripcion?.split('\\.')?.take(idx ? 1 : 3)?.join('.') ?: ''
            String suffix = item.descripcion.size() > size ? '...' : ''
            item.descripcion = item.descripcion.take(size) + suffix
            if (idx) {
                item.distancia = metersTo(list[idx - 1].latitud, list[idx - 1].longitud, list[idx].latitud, list[idx].longitud)
            }
        }
        starting.recorrido = list.sum { it.distancia ?: 0 }
        starting.etiquetas = []
        if (starting.recorrido > 4000)
            starting.etiquetas << 'dificil'
        else if (starting.recorrido > 2000)
            starting.etiquetas << 'media'
        else
            starting.etiquetas << 'facil'

        list
    }

    void init() {
        def edificiosURL = 'https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=xml&file=0&filename=208844-0-monumentos-edificios&mgmtid=69e30c658f6a8410VgnVCM2000000c205a0aRCRD&preview=full'
        if (!Files.exists(Path.of('data/edificios.xml'))) {
            new File('data/edificios.xml').bytes = edificiosURL.toURL().bytes
        }
        def edificiosXml = new XmlParser().parse('data/edificios.xml')

        edificiosXml.contenido.each {
            def pojo = [:]
            pojo.id = 'ed' + it.atributos.atributo.find { it.@nombre == 'ID-EDIFICIO' }.text()
            pojo.nombre = it.atributos.atributo.find { it.@nombre == 'NOMBRE' }.text()
            pojo.url = it.atributos.atributo.find { it.@nombre == 'CONTENT-URL' }.text()
            def localizacion = it.atributos.atributo.find { it.@nombre == 'LOCALIZACION' }
            if (!localizacion)
                return
            pojo.latitud = localizacion.atributo.find { it.@nombre == 'LATITUD' }?.text()
            if (!pojo.latitud)
                return
            pojo.longitud = localizacion.atributo.find { it.@nombre == 'LONGITUD' }?.text()
            if (!pojo.longitud)
                return
            pojo.latitud = pojo.latitud as float
            pojo.longitud = pojo.longitud as float
            pojo.direccion = localizacion.atributo.find { it.@nombre == 'CLASE-VIAL' }?.text() ?: ''
            pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'NOMBRE-VIA' }?.text() ?: '')
            pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'NUM' }?.text() ?: '')
            pojo.direccion = pojo.direccion.trim()
            pojos << pojo
        }

        def monumentosURL = 'https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=xml&file=0&filename=300356-0-monumentos-ciudad-madrid&mgmtid=eb8e993ae322b610VgnVCM1000001d4a900aRCRD&preview=full'
        if (!Files.exists(Path.of("data/monumentos.xml"))) {
            new File('data/monumentos.xml').bytes = monumentosURL.toURL().bytes
        }
        def monumentosXML = new XmlParser().parse('data/monumentos.xml')

        monumentosXML.contenido.each {
            def pojo = [:]
            pojo.id = 'mn' + it.atributos.atributo.find { it.@nombre == 'ID-MONUMENTO' }.text()
            pojo.nombre = it.atributos.atributo.find { it.@nombre == 'NOMBRE' }.text()
            pojo.descripcion = it.atributos.atributo.find { it.@nombre == 'DESCRIPCION' }?.text() ?: ''
            pojo.descripcion = pojo.descripcion.replaceAll('\u0096', '-')
            pojo.url = it.atributos.atributo.find { it.@nombre == 'CONTENT-URL' }?.text() + '&vgnextfmt=default'
            def localizacion = it.atributos.atributo.find { it.@nombre == 'LOCALIZACION' }
            if (!localizacion)
                return
            pojo.latitud = localizacion.atributo.find { it.@nombre == 'LATITUD' }?.text()
            if (!pojo.latitud)
                return
            pojo.longitud = localizacion.atributo.find { it.@nombre == 'LONGITUD' }?.text()
            if (!pojo.longitud)
                return
            pojo.latitud = pojo.latitud as float
            pojo.longitud = pojo.longitud as float
            pojo.direccion = localizacion.atributo.find { it.@nombre == 'CLASE-VIAL' }?.text() ?: ''
            pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'NOMBRE-VIA' }?.text() ?: '')
            pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'NUM' }?.text() ?: '')
            if (!pojo.direccion.trim()) {
                pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'BARRIO' }?.text() ?: '')
                pojo.direccion += ' ' + (localizacion.atributo.find { it.@nombre == 'DISTRITO' }?.text() ?: '')
            }
            pojo.etiquetas = []
            pojo.categoria = pojo.id.startsWith('mn') ? 'monumento' : 'edificio'
            pojos << pojo
        }
    }
}