package util

import java.nio.file.Files
import java.nio.file.Path

def model(List list){
    Map.of(
            'today', new Date().format('yyyy-MM-dd'),
            'starting', list.first(),
            'list', list.drop(1),
            'baseURL','https://blog.rutas-por-madrid.es'
    )
}

def thumbFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("mapa-${id}.png")
    ret
}

def imageFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("${id}.jpg")
    ret
}

def fichaFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("${id}.adoc")
    ret
}

def mapaFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("ruta-${id}.html")
    ret
}

def letterFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("letter-${id}.html")
    ret
}
def blogFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("${id}.md")
    ret
}
def telegramFile(String id){
    def buildDir = Files.exists(Path.of('build')) ? Path.of('build') : Files.createDirectory(Path.of('build'))
    def path = Path.of('build').resolve(id)
    Files.createDirectories(path)
    def ret = path.resolve("telegram-${id}.groovy")
    ret
}
