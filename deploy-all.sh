#!/bin/bash
mkdir -p site/static/images
mkdir -p site/static/maps
mkdir -p site/static/newsletter
mkdir -p site/static/pdf
#!/bin/bash
for d in build/*; do
  d=""${d##*/}""
  echo "$d"
  cp build/$d/*.jpg site/static/images
  cp build/$d/*.png site/static/images
  cp build/$d/ruta-${d}.html site/static/maps
  cp build/$d/${d}.pdf site/static/pdf
  cp build/$d/${d}.md site/content/posts
done